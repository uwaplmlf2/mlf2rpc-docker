FROM debian:jessie
MAINTAINER Mike Kenney <mikek@apl.uw.edu>

# runit depends on /etc/inittab which is not present in debian:jessie
RUN touch /etc/inittab
RUN apt-get update && apt-get -y install \
        curl \
        libhdf5-dev \
        libnetcdf-dev \
        python-dev \
        python-numpy \
        python-matplotlib \
        python-setuptools \
        python-scipy \
        python-tz \
        python-pip \
        runit \
        && \
        apt-get -y autoremove && \
        apt-get clean

WORKDIR /
COPY requirements.txt /

# Prebuilt wheel for netCDF4 to avoid having to install compilers
COPY netCDF4-1.2.0-cp27-none-linux_x86_64.whl /

# Install the required Python packages
RUN  \
      pip install netCDF4-1.2.0-cp27-none-linux_x86_64.whl && \
      rm -f netCDF4-1.2.0-cp27-none-linux_x86_64.whl && \
      pip install ply && \
      pip install -r /requirements.txt

# Copy the applications
COPY ./apps/* /usr/bin/
# Copy the service scripts
COPY ./etc /etc/
COPY entrypoint.sh /

# Base directory for the service script logs
RUN mkdir -p /var/log/mlf2
VOLUME ["/var/log/mlf2"]

ENV AMQP_SERVER mlf2data.apl.uw.edu

ENTRYPOINT ["/entrypoint.sh"]
