#!/bin/bash
#

if [[ -z "$1" ]]; then
    # Dump all of the env variables. We must to this because
    # runit will not pass the env variables to the service
    # scripts. Each script must source this file.
    export > /etc/envvars
    # Run the service manager
    exec /usr/sbin/runsvdir-start
else
    exec "$@"
fi
