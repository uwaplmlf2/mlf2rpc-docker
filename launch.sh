#!/bin/bash

[[ -z "$1" ]] && {
    echo "Usage: $(basename $0) credentials" 1>&2
    exit 1
}

IFS=: read -a creds < "$1"

id="$(docker ps -q -f name=mlf2-rpc)"
[[ -n $id ]] && {
    docker stop $id
    docker rm $id
}

oldim="$(docker images -q mfk/mlf2rpc)"
addr=$(ip addr show docker0 | awk '/inet / {split($2,s,"/");print s[1]}')

docker build --force-rm=true -t  mfk/mlf2rpc . && \
    [[ -n "$oldim" ]] && docker rmi "$oldim"

docker run --restart=on-failure --name mlf2-rpc \
       -v /var/log/mlf2:/var/log/mlf2 \
       -e MAILHOST=$addr \
       -e MQ_USER=${creds[0]} \
       -e MQ_PASSWORD=${creds[1]} \
       -d mfk/mlf2rpc
docker inspect -f '{{.Config.Env}}' mlf2-rpc
