# Docker Container for MLF2 RPC services

Installs the following AMQ-based RPC services used to process incoming
MLF2 data files and message replies.

## Services

### sexp_rpc

Converts an S-expression format data file into a ZIP archive of netCDF
files.

### plot_rpc

Creates a PNG-format data plot image from a netCDF environmental data
file.

### email_reply

Forwards a message response to the sender's email address. Needs to be
supplied with a mail host via the `MAILHOST` environment variable.

## Installation

```
docker build --force-rm=true -t mfk/mlf2rpc .
```

## Usage

In the example below $LOGDIR is assumed to contain the name of a local
directory for the service log files. All of the environment variables
shown must be specified:

```
docker run --restart=on-failure --name mlf2-rpc \
    -v $LOGDIR:/var/log/mlf2 \
    -e MAILHOST=172.17.42.1 \
    -e MQ_USER=<AMQP server username> \
    -e MQ_PASSWORD=<password> \
    -d mfk/mlf2rpc
```
