#!/usr/bin/env python
#
# AMQP based RPC service to create a summary plot (PNG format) from an
# MLF2 environmental (env) file.
#
"""
%prog [options]

AMQP based RPC service to create a summary plot of an MLF2 env file.
"""
import sys
import signal
import pika
import logging
import os
from tempfile import mkstemp
from optparse import OptionParser
from mlf2data import plotting
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

BP = pika.BasicProperties


def signal_handler(signum, frame):
    sys.exit(0)


def create_plot_image(datafile, name):
    """
    Create the plot and save to a PNG image.

    @datafile: filesystem path of input data file
    @name: name of input data file.
    @return a tuple of file-path, name, mime-type for image file
    """
    fd, filepath = mkstemp(suffix='.png')
    os.close(fd)
    fig = Figure(figsize=(8, 6))
    plotting.summary_plot(datafile, fig, 10)
    canvas = FigureCanvasAgg(fig)
    canvas.print_figure(filepath, dpi=96)
    base, _ = os.path.splitext(name)
    return filepath, base+'.png', 'image/png'


def process_file(contents, options):
    filename = options.get('filename', 'data.nc')
    logging.info('Processing %s', filename)
    fd, filepath = mkstemp(suffix='.nc')
    os.write(fd, contents)
    os.close(fd)
    try:
        imagepath, name, mimetype = create_plot_image(filepath, filename)
    except Exception as e:
        result = ''
        mimetype = 'text/plain'
        options['error'] = str(e)
    else:
        result = open(imagepath, 'rb').read()
        os.unlink(imagepath)
        options['filename'] = name
    os.unlink(filepath)
    return mimetype, result


def on_request(ch, method, props, body):
    """
    Handle the file processing request. The file contents are contained in
    the body of the message.
    """
    headers = props.headers
    mimetype, response = process_file(body, headers)
    ctype = response and mimetype or 'text/plain'
    ch.basic_publish(exchange=method.exchange,
                     routing_key=props.reply_to,
                     properties=BP(correlation_id=props.correlation_id,
                                   content_type=ctype,
                                   headers=headers),
                     body=response)
    # For AMQP replies, we always want to remove the input message from
    # the queue, either by acknowledgment or rejection.
    if response:
        ch.basic_ack(delivery_tag=method.delivery_tag)
    else:
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds,
                                       heartbeat_interval=300)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    logging.info('Connected to %s', host)
    return channel


def mainloop(server, user, pword, exchange):
    qname = 'env_plot'
    channel = mq_setup(server, (user, pword))
    channel.exchange_declare(exchange=exchange,
                             durable=True,
                             type='topic')
    channel.queue_declare(queue=qname,
                          durable=True,
                          exclusive=False,
                          auto_delete=False)
    channel.queue_bind(exchange=exchange,
                       queue=qname,
                       routing_key=qname)

    channel.basic_consume(on_request, queue=qname, no_ack=False)
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        channel.start_consuming()
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Aborting')
    finally:
        channel.close()


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='localhost', credentials=None,
                        exchange='data')
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='HOST',
                      help='specify AMQP server host (default: %default)')
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='USER:PASSWORD',
                      help='credentials file for server access')
    parser.add_option('-e', '--exchange',
                      type='string',
                      dest='exchange',
                      metavar='NAME',
                      help='AMQP exchange to use (default: %default)')

    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')
    opts, args = parser.parse_args()
    if opts.credentials:
        if opts.credentials.startswith('@'):
            with open(opts.credentials[1:], 'r') as f:
                user, pword = f.readline().strip().split(':')
        else:
            user, pword = opts.credentials.split(':')
    else:
        user, pword = 'guest', 'guest'

    mainloop(opts.server, user, pword, opts.exchange)

if __name__ == '__main__':
    main()
