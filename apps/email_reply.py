#!/usr/bin/env python
#
"""
%prog [options] amqp_server

Monitor an AMQP message queue for replies from an MLF2 float and email the
contents to the original sender. The sender's email address must be stored
in the reply_to message header.
"""
import sys
import signal
import pika
import smtplib
import logging
import logging.handlers
import os
from email.mime.text import MIMEText
from optparse import OptionParser


def signal_handler(signum, frame):
    sys.exit(0)


def handle_reply(ch, method, properties, body):
    """
    AMQP consumer function that forwards the reply message via email.
    """
    logger = logging.getLogger()
    sender = 'mlf2-noreply@apl.uw.edu'
    headers = properties.headers
    msg = MIMEText(str(body))
    msg['To'] = properties.reply_to
    msg['From'] = sender
    msg['Subject'] = 'reply from float-%d' % headers['floatid']
    try:
        s = smtplib.SMTP(os.environ.get('MAILHOST', 'localhost'))
        logger.info('Forwarding reply to %s', properties.reply_to)
        s.sendmail(sender, [properties.reply_to], msg.as_string())
        s.quit()
        ch.basic_ack(delivery_tag=method.delivery_tag)
    except Exception:
        # It's hard to separate transient errors from permanent ones
        # so to avoid getting stuck in an endless loop, reject the
        # message and do not requeue it.
        logger.exception('Delivery failed to %s', properties.reply_to)
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds,
                                       heartbeat_interval=300)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    logging.info('Connected to %s', host)
    return channel


def mainloop(server, user, pword):
    channel = mq_setup(server, (user, pword))
    channel.queue_declare(queue='email_reply', durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(handle_reply, queue='email_reply')
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        channel.start_consuming()
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Aborting')
    finally:
        channel.close()


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(credentials=None)
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='USER:PASS',
                      help='username and password for AMQP server')

    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing AMQP server name')

    if opts.credentials:
        if opts.credentials.startswith('@'):
            with open(opts.credentials[1:], 'r') as f:
                user, pword = f.readline().strip().split(':')
        else:
            user, pword = opts.credentials.split(':')
    else:
        user, pword = 'guest', 'guest'

    mainloop(args[0], user, pword)


if __name__ == '__main__':
    main()
