#!/usr/bin/env python
#
# AMQP based RPC service to convert an MLF2 s-exp data file to a ZIP
# archive of netcdf files. The MLF2 s-exp files are a restricted form of
# the s-exp standard in which each '\n' delimited line of the file is a
# separate expression.
#
"""
%prog [options]

AMQP based RPC service to convert and MLF2 s-expression datafile to a ZIP
archive of netcdf files, one file per sensor.
"""
import sys
import signal
import pika
import logging
import string
import os
import re
from zipfile import ZipFile, ZIP_DEFLATED
from mlf2sexp import sexp, extract
from cStringIO import StringIO
from contextlib import contextmanager
from tempfile import mkdtemp
from shutil import rmtree
from optparse import OptionParser

BP = pika.BasicProperties


def signal_handler(signum, frame):
    sys.exit(0)


@contextmanager
def pushd(dirname):
    """
    Context manager to temporarily work in a new directory
    """
    cwd = os.getcwd()
    os.chdir(dirname)
    try:
        yield
    finally:
        os.chdir(cwd)


def next_line(contents):
    """
    Iterate over the lines of an s-exp file stored in a string
    """
    infile = StringIO(contents)
    remove = string.whitespace + '\x1a'
    for line in infile:
        line = line.rstrip(remove)
        if line and line.endswith(')'):
            yield line


def convert_file(contents, options):
    """
    Convert the contents of a s-exp file to a ZIP archive of netcdf files
    (one file per sensor). The ZIP contents are returned as a string.
    """
    workdir = mkdtemp()
    tag = ''
    archive_name = 'data.zip'
    ncattrs = {}
    filename = options.get('filename')
    if filename:
        name, ext = os.path.splitext(filename)
        archive_name = name + '.zip'
        m = re.match(r'[a-z]+(\d+)', name, re.IGNORECASE)
        if m:
            tag = '_' + m.group(1)
    logging.info('Processing S-exp file %s', filename or '<unnamed>')
    if 'floatid' in options:
        ncattrs['floatid'] = options['floatid']
    with pushd(workdir):
        d = extract.Database(archive=ZipFile(archive_name, 'w', ZIP_DEFLATED),
                             ncattrs=ncattrs,
                             nametag=tag)
        p = sexp.Parser()
        recnum = 1
        try:
            for line in next_line(contents):
                d.addrec(p.parse_exp(line))
                recnum += 1
            logging.info('Processed %d records', recnum)
        except SyntaxError:
            result = ''
            options['error'] = 'Syntax error in record %d' % recnum
            logging.error(options['error'])
        else:
            summary = d.close()
            logging.info('Archive contents %s', str(summary))
            result = open(archive_name, 'rb').read()
    rmtree(workdir)
    options['filename'] = archive_name
    return result


def on_request(ch, method, props, body):
    """
    Handle the file conversion request. The file contents are contained in
    the body of the message.
    """
    headers = props.headers
    logging.info('Message properties: %s', str(props))
    response = convert_file(body, headers)
    ctype = response and 'application/zip' or 'text/plain'
    ch.basic_publish(exchange=method.exchange,
                     routing_key=props.reply_to,
                     properties=BP(correlation_id=props.correlation_id,
                                   content_type=ctype,
                                   headers=headers),
                     body=response)
    # For AMQP replies, we always want to remove the input message from
    # the queue, either by acknowledgment or rejection.
    if response:
        ch.basic_ack(delivery_tag=method.delivery_tag)
    else:
        ch.basic_reject(delivery_tag=method.delivery_tag, requeue=False)


def mq_setup(host, credentials):
    creds = pika.PlainCredentials(*credentials)
    params = pika.ConnectionParameters(host=host,
                                       virtual_host='mlf2',
                                       credentials=creds,
                                       heartbeat_interval=300)
    conn = pika.BlockingConnection(params)
    channel = conn.channel()
    logging.info('Connected to %s', host)
    return channel


def mainloop(server, user, pword, exchange):
    qname = 'sexp_convert'
    channel = mq_setup(server, (user, pword))
    channel.exchange_declare(exchange=exchange,
                             durable=True,
                             type='topic')
    channel.queue_declare(queue=qname,
                          durable=True,
                          exclusive=False,
                          auto_delete=False)
    channel.queue_bind(exchange=exchange,
                       queue=qname,
                       routing_key=qname)

    channel.basic_consume(on_request, queue=qname, no_ack=False)
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        channel.start_consuming()
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Aborting')
    finally:
        channel.close()


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(server='localhost', credentials=None,
                        exchange='data')
    parser.add_option('-s', '--server',
                      type='string',
                      dest='server',
                      metavar='HOST',
                      help='specify AMQP server host (default: %default)')
    parser.add_option('-c', '--credentials',
                      type='string',
                      dest='credentials',
                      metavar='USER:PASSWORD',
                      help='credentials file for server access')
    parser.add_option('-e', '--exchange',
                      type='string',
                      dest='exchange',
                      metavar='NAME',
                      help='AMQP exchange to use (default: %default)')

    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)-8s %(message)s')
    opts, args = parser.parse_args()
    if opts.credentials:
        if opts.credentials.startswith('@'):
            with open(opts.credentials[1:], 'r') as f:
                user, pword = f.readline().strip().split(':')
        else:
            user, pword = opts.credentials.split(':')
    else:
        user, pword = 'guest', 'guest'

    mainloop(opts.server, user, pword, opts.exchange)

if __name__ == '__main__':
    main()
